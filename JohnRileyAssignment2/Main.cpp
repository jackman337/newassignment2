#include <iostream>
#include <conio.h>

float Add(float num1, float num2) {
	return num1 + num2;
}

float Subtract(float num1, float num2) {
	return num1 - num2;
}

float Divide(float num1, float num2) {
	return num1 / num2;
}

float Multiply(float num1, float num2) {
	return num1 * num2;
}


int main() {
	float num1, num2;
	char Operand;
	bool answer = true;

	std::cout << "Please Enter +, -, / or *: ";
	std::cin >> Operand;

	std::cout << "Please Enter two Numbers ";
	std::cin >> num1 >> num2;

	if (Operand == '+')
		std::cout << Add(num1, num2);
		
	else if (Operand == '-')
		std::cout << Subtract(num1, num2);
		
	else if (Operand == '/')
		std::cout << Divide(num1, num2);
		
	else if (Operand == '*')
		std::cout << Multiply(num1, num2);

	std::cout << "Would You like to play again?";
	std::cin >> answer;

	if (answer = 'yes')
		answer = true;

	else if (answer = 'no')
		answer = false;

	_getch();
	return 0;
}